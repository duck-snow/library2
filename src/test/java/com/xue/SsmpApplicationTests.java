package com.xue;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xue.dao.BookDao;
import com.xue.pojo.Books;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SsmpApplicationTests {


    @Autowired
    private BookDao bookDao;

    @Test
    void testGetById(){
        System.out.println(bookDao.selectById(1));
    }

    @Test
    void testGetBy(){
        LambdaQueryWrapper<Books> lqw = new LambdaQueryWrapper<>();
        lqw.like(Books::getName,"java");
        bookDao.selectList(lqw);
    }

}
