package com.xue.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xue.pojo.Books;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.xml.ws.soap.Addressing;

/**
 * @author xue
 * @Classname BookServiceTestCase
 * @Date 2022/5/12 10:51
 */
@SpringBootTest
public class BookServiceTestCase {


    @Autowired
    private IBookService bookService;

    @Test
    void testGetById(){
        System.out.println(bookService.getById(1));
    }
    @Test
    void testGetPage(){
        IPage<Books> page = new Page<>(1,1);
         bookService.page(page);

        System.out.println(page.getCurrent());
        System.out.println(page.getSize());
        System.out.println(page.getTotal());
        System.out.println(page.getPages());
        System.out.println(page.getRecords());

    }
    @Test
    void testGetAll(){
        System.out.println(bookService.list());
    }

}
