package com.xue.pojo;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author xueyueqing
 */
@Data
@TableName("t_book")
public class Books {

    private Integer id;
    private String type;
    private String name;
    private String description;

}
