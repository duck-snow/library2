package com.xue.controller.utils;

import lombok.Data;

/**
 * @author xue
 * @Classname R
 * @Date 2022/5/13 20:24
 */
@Data
public class ResultMessage {

    private Boolean flag;
    private Object data;

    private String message;

    public ResultMessage(){

    }
    public ResultMessage(Boolean flag){
        this.flag = flag;
    }

    public ResultMessage(Boolean flag, Object data){
        this.flag = flag;
        this.data = data;
    }


    public ResultMessage(String message){
        this.flag = flag;
        this.message = message;
    }

    public ResultMessage(Boolean flag, String message){
        this.flag = flag;
        this.message = message;
    }
}
