package com.xue.controller.utils;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author xue
 * @Classname ExceptionAdvice
 * @Date 2022/5/15 15:20
 * 作为springmvc的异常处理器
 */
@RestControllerAdvice
public class ExceptionAdvice {

    //拦截异常
    @ExceptionHandler
    public ResultMessage doException(Exception e){
        e.printStackTrace();
        return new ResultMessage("服务器故障,稍后再试");
    }

}
