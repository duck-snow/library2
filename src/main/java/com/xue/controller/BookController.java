package com.xue.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xue.controller.utils.ResultMessage;
import com.xue.pojo.Books;
import com.xue.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Currency;
import java.util.List;

/**
 * @author xue
 * @Classname BookController
 * @Date 2022/5/13 17:05
 */


@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private IBookService bookService;

    @GetMapping
    public ResultMessage getAll() {
        return new ResultMessage(true,bookService.list());
    }

    @PostMapping
    public ResultMessage save(@RequestBody Books books) {
     return new ResultMessage(bookService.saveBook(books));

    }

    @DeleteMapping("{id}")
    public ResultMessage delete(@PathVariable Integer id) {
        return new ResultMessage(bookService.delete(id));
    }

    @PutMapping
    public ResultMessage update(@RequestBody Books books) {
        return  new ResultMessage(bookService.modify(books));
    }

    @GetMapping("{id}")
    public ResultMessage getById(@PathVariable Integer id) {
        //true表示查询到数据了 可能为空
        return new ResultMessage(true,bookService.getById(id));
    }

    /**
     * 分页  条件查询
     * @param currentPage
     * @param pageSize
     * @param books
     * @return
     */

    @GetMapping("{currentPage}/{pageSize}")
    public ResultMessage getPage(@PathVariable int currentPage, @PathVariable int pageSize, Books books) {
          IPage<Books> page = bookService.getPage(currentPage, pageSize,books);
          //解决删除某页唯一一条数据时，出现空页面的问题
          if (currentPage > page.getPages()){
              page = bookService.getPage((int) page.getPages(),pageSize,books);
          }
        return new ResultMessage(true, page);
    }

}
