package com.xue.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xue.pojo.Books;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author xue
 * @Classname BookDao
 * @Date 2022/5/10 21:07
 */


@Mapper
public interface BookDao extends BaseMapper<Books> {


}
