package com.xue.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xue.dao.BookDao;
import com.xue.pojo.Books;
import com.xue.service.IBookService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author xue
 * @Classname BookServiceImpl
 * @Date 2022/5/12 10:34
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookDao,Books> implements IBookService {

    @Autowired
    private BookDao bookDao;

    @Override
    public boolean saveBook(Books books) {
        return bookDao.insert(books) > 0;
    }

    @Override
    public boolean modify(Books books) {
        return bookDao.updateById(books) > 0;
    }

    @Override
    public boolean delete(Integer id) {
        return  bookDao.deleteById(id) > 0;
    }

    @Override
    public IPage<Books> getPage(int currentPage, int pageSize) {
       IPage page = new Page(currentPage,pageSize);

       bookDao.selectPage(page,null);
       return page;
    }

    @Override
    public IPage<Books> getPage(int currentPage, int pageSize, Books books) {
        LambdaQueryWrapper<Books> lqw = new LambdaQueryWrapper<>();
        lqw.like(Strings.isNotEmpty(books.getType()), Books::getType,books.getType());
        lqw.like(Strings.isNotEmpty(books.getName()), Books::getName,books.getName());
        lqw.like(Strings.isNotEmpty(books.getDescription()), Books::getDescription,books.getDescription());
        IPage page = new Page(currentPage,pageSize);

        bookDao.selectPage(page,lqw);
        return page;

    }
}
