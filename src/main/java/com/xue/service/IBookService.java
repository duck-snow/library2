package com.xue.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xue.pojo.Books;
import org.springframework.stereotype.Service;

import java.awt.print.Book;

/**
 * @author xue
 * @Classname IBookService
 * @Date 2022/5/12 11:15
 */

public interface IBookService extends IService<Books> {

  boolean saveBook(Books books);

  boolean modify(Books books);

  boolean delete(Integer id);

  IPage<Books> getPage(int currentPage, int pageSize);

  IPage<Books> getPage(int currentPage, int pageSize,Books books);

}
