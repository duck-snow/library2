/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.36-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `t_book` (
	`id` int (11),
	`type` varchar (60),
	`name` varchar (60),
	`description` varchar (60)
); 
insert into `t_book` (`id`, `type`, `name`, `description`) values('1','计算机基础','Java','Java核心技术');
insert into `t_book` (`id`, `type`, `name`, `description`) values('2','计算机基础','操作系统','详解操作系统');
insert into `t_book` (`id`, `type`, `name`, `description`) values('3','计算机基础','Spring','Spring详解与实战');
insert into `t_book` (`id`, `type`, `name`, `description`) values('4','哲学','思修','思想道德修养');
insert into `t_book` (`id`, `type`, `name`, `description`) values('5','测试数据123','测试数据2','测试数据3');
insert into `t_book` (`id`, `type`, `name`, `description`) values('6','计算机基础','深入理解jvm','深入理解jvm底层原理及开发');
insert into `t_book` (`id`, `type`, `name`, `description`) values('9','dadadddas','dasd','dasd');
insert into `t_book` (`id`, `type`, `name`, `description`) values('10','dadasd','42342','de24');
insert into `t_book` (`id`, `type`, `name`, `description`) values('11','4234','423','dasda');
insert into `t_book` (`id`, `type`, `name`, `description`) values('15','dasd','dasd','dasd');
insert into `t_book` (`id`, `type`, `name`, `description`) values('16','4123123','dasd','dasd');
