# library2

## 介绍
基于SpringBoot+Mybatis-Plus 的入门案例——图书管理系统

图书管理系统 

### 1. 所用技术栈和环境配置

- SpringBoot 2.6.7
- Mybatis-Plus 3.4.3
- lombok 1.18
- mysql 5.6
- JDK 8

### 2.案例效果演示

- 2.1添加书籍

![image-20220515171305431](https://gitee.com/duck-snow/picture/raw/master/img/202205151713665.png)

- 2.2 删除书籍

![image-20220515171335591](https://gitee.com/duck-snow/picture/raw/master/img/202205151713659.png)

- 2.3 修改书籍

![image-20220515171400862](https://gitee.com/duck-snow/picture/raw/master/img/202205151714938.png)

- 2.4 按条件查询书籍

![image-20220515171424577](https://gitee.com/duck-snow/picture/raw/master/img/202205151714637.png)

- 2.5分页

![image-20220515171456800](https://gitee.com/duck-snow/picture/raw/master/img/202205151714852.png)

